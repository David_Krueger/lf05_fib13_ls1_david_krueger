public class Raumschiff {
  // Attribute
  private int photonentorpedoAnzahl;
  private int energieversorgungInProzent;
  private int schildeInProzent;
  private int huelleInProzent;
  private int lebenserhaltungssystemeInProzent;
  private int androidenAnzahl;
  private String schiffsname;
  private ArrayList<String> broadcastKommunikator;
  private ArrayList<ladung_4_1> ladungsverzeichnis;

  // Konstruktoren
  public raumschiff () {}

  public raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, 
    int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
    ArrayList<String> broadcastKommunikator, ArrayList<ladung_4_2> ladungsverzeichnis) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl; 
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
        this.broadcastKommunikator = broadcastKommunikator;
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

  // Verwaltungsmethoden - setter
  public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
  }

  public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
    this.energieversorgungInProzent = energieversorgungInProzent;
  }

  public void setSchildeInProzent(int schildeInProzent) {
    this.schildeInProzent = schildeInProzent;
  }

  public void setHuelleInProzent(int huelleInProzent) {
    this.huelleInProzent = huelleInProzent;
  }

  public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
    this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
  }

  public void setAndroidenAnzahl(int androidenAnzahl) {
    this.androidenAnzahl = androidenAnzahl;
  }

  public void setSchiffsname(String schiffsname) {
    this.schiffsname = schiffsname;
  }

  public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
    this.broadcastKommunikator = broadcastKommunikator;
  }

  public void setLadungsverzeichnis(ArrayList<ladung_4_2> ladungsverzeichnis) {
    this.ladungsverzeichnis = ladungsverzeichnis;
  }

  // Verwaltungsmethoden - getter
  public String getPhotonentorpedoAnzahlan() {
    return this.photonentorpedoAnzahl;
  }

  public String getEnergieversorgungInProzent() {
    return this.energieversorgungInProzent;
  }

  public String getSchildeInProzent() {
    return this.schildeInProzent;
  }

  public String getHuelleInProzent() {
    return this.huelleInProzent;
  }

  public String getLebenserhaltungssystemeInProzent() {
    return this.lebenserhaltungssystemeInProzent;
  }

  public String getAndroidenAnzahl() {
    return this.androidenAnzahl;
  }

  public String getSchiffsname() {
    return this.schiffsname;
  }

  public String getBroadcastKommunikator() {
    return this.broadcastKommunikator;
  }

  public String getLadungsverzeichnis() {
    return this.ladungsverzeichnis;
  }

  // allgemeine Methoden
  public void addLadung(ladung_4_2 neueLadung) {
    // leer
  }
}
