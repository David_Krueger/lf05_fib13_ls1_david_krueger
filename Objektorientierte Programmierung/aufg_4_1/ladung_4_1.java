public class Ladung {
    // Attribute
    private String bezeichnung;
    private int menge;

    // Konstruktoren
    public Ladung() {
    }

    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    // Verwaltungsmethoden - setter
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    // Verwaltungsmethoden - getter
    public String getBezeichnung() {
        return this.bezeichnung;
    }

    public int getMenge() {
        return this.menge;
    }
}
