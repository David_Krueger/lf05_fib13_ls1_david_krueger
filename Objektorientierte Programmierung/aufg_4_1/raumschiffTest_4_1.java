public class RaumschiffTest {

    public static void main(String[] args) {

        // Objekterzeugung
        Raumschiff klingonen = new Raumschiff();

        Ladung kling1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung kling2 = new Ladung("Bat'leth Klingonen Schwert", 200);

        Raumschiff romulaner = new Raumschiff();

        Ladung rom1 = new Ladung("Borg-Schrott", 5);
        Ladung rom2 = new Ladung("Rote Materie", 2);
        Ladung rom3 = new Ladung("Plasma-Waffe", 50);

        Raumschiff vulkanier = new Raumschiff();

        Ladung vulk1 = new Ladung("Forschungssonde", 35);
        Ladung vulk2 = new Ladung("Photonentorpedo", 3);

        // Obejektmanipulation
        klingonen.setTorpedos(1);
        klingonen.setEnergie(100);
        klingonen.setSchutzschilde(100);
        klingonen.setHuelle(100);
        klingonen.setLebenserhaltung(100);
        klingonen.setName("IKS Hegh'ta");
        klingonen.setReparaturBots(2);

        romulaner.setTorpedos(2);
        romulaner.setEnergie(100);
        romulaner.setSchutzschilde(100);
        romulaner.setHuelle(100);
        romulaner.setLebenserhaltung(100);
        romulaner.setName("IRW Khazara");
        romulaner.setReparaturBots(2);

        vulkanier.setTorpedos(0);
        vulkanier.setEnergie(80);
        vulkanier.setSchutzschilde(80);
        vulkanier.setHuelle(50);
        vulkanier.setLebenserhaltung(100);
        vulkanier.setName("Ni'Var");
        vulkanier.setReparaturBots(5);
    }
}