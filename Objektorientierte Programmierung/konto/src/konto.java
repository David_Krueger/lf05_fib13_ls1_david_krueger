public class Konto {
	// Attribute
	private String iban;
	private int kontonr;
	private double kontostand;

	// Konstruktoren
	public Konto() {
	}

	public Konto(String iban, int kontonr, double kontostand) {
		this.iban = iban;
		this.kontostand = kontostand;
		this.kontonr = kontonr;
	}

	// Verwaltungsmethoden - setter
	public void setIban(String iban) {
		this.iban = iban;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}

	public void setKontonr(int kontonr) {
		this.kontonr = kontonr;
	}

	// Verwaltungsmethoden - getter
	public String getIban() {
		return this.iban;
	}

	public double getKontostand() {
		return this.kontostand;
	}

	public int getKontonr() {
		return this.kontonr;
	}

	// allgemeine Methoden
	public void geldAbheben(double menge) {
		this.kontostand -= menge;
	}

	public void geldEinzahlen(double menge) {
		this.kontostand += menge;
	}

	public void geldUeberweisen(double menge, Konto ziel) {
		this.kontostand -= menge;
		ziel.geldEinzahlen(menge);
	}
}
