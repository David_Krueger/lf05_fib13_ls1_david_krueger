public class KontoTest {

	public static void main(String[] args) {
		// Schritt 1 Objekte instanzen erstellen
		Konto k1 = new Konto();
		Konto k2 = new Konto("DE987", 0002, 100.19);
		Besitzer b1 = new Besitzer();

		// Objektmanipulierung
		k1.setIban("DE123");
		k1.setKontonr(0001);
		k1.setKontostand(10.99);

		b1.setVorname("Testname");
		b1.setNachname("Nachname!!");
		b1.setKonto1(k1);
		b1.setKonto2(k2);

		// Methoden Test
		k1.geldUeberweisen(30.00, k2);

		// Konto Ausgaben
		System.out.println("-- Konto --");
		System.out.println("IBAN: " + k1.getIban());
		System.out.println("KARTENNUMMER: " + k1.getKontonr());
		System.out.println("KONTOSTAND: " + k1.getKontostand());

		System.out.println("---");

		System.out.println("IBAN: " + k2.getIban());
		System.out.println("KARTENNUMMER: " + k2.getKontonr());
		System.out.println("KONTOSTAND: " + k2.getKontostand());

		// Besitzer Ausgaben
		System.out.println("\n-- Inhaber --");
		System.out.println("Vorname: " + b1.getVorname());
		System.out.println("Nachname: " + b1.getNachname());
		System.out.println("Konto X: " + b1.getKonto1());
		System.out.println("Konto Y: " + b1.getKonto2());

		b1.gesamtUebersicht();
		System.out.println("Gesamt: " + b1.gesamtGeld() + "€");

	}

}
