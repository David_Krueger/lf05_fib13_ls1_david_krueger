/**
 * @author David Krüger
 * @version 1.0
 */

import java.util.ArrayList;

public class raumschiffTest_4_2 {

    /**
     * Main Methode der Testklasse, gibt entsprechende Werte auf der Konsole aus
     * 
     * @param String, arguments. Keine Notwendigkeit
     */
    public static void main(String[] args) {

        // Objekterzeugung
        Raumschiff_4_2 klingonen = new Raumschiff_4_2();
        
        Ladung_4_2 kling1 = new Ladung_4_2("Ferengi Schneckensaft", 200);
        Ladung_4_2 kling2 = new Ladung_4_2("Bat'leth Klingonen Schwert", 200);

        Raumschiff_4_2 romulaner = new Raumschiff_4_2();

        Ladung_4_2 rom1 = new Ladung_4_2("Borg-Schrott", 5);
        Ladung_4_2 rom2 = new Ladung_4_2("Rote Materie", 2);
        Ladung_4_2 rom3 = new Ladung_4_2("Plasma-Waffe", 50);

        Raumschiff_4_2 vulkanier = new Raumschiff_4_2();

        Ladung_4_2 vulk1 = new Ladung_4_2("Forschungssonde", 35);
        Ladung_4_2 vulk2 = new Ladung_4_2("Photonentorpedo", 3);

        // Obejektmanipulation
        klingonen.setphotonentorpedo(1);
        klingonen.setEnergieversorgungInProzent(100);
        klingonen.setSchildeInProzent(100);
        klingonen.setHuelleInProzent(100);
        klingonen.setLebenserhaltungssystemeInProzent(100);
        klingonen.setSchiffsname("IKS Hegh'ta");
        klingonen.setReparaturBots(2);
        klingonen.addLadung(kling1);
		klingonen.addLadung(kling2);

        romulaner.setphotonentorpedo(2);
        romulaner.setEnergieversorgungInProzent(100);
        romulaner.setSchildeInProzent(100);
        romulaner.setHuelleInProzent(100);
        romulaner.setLebenserhaltungssystemeInProzent(100);
        romulaner.setSchiffsname("IRW Khazara");
        romulaner.setReparaturBots(2);
        romulaner.addLadung(rom1);
		romulaner.addLadung(rom2);
		romulaner.addLadung(rom3);

        vulkanier.setphotonentorpedo(0);
        vulkanier.setEnergieversorgungInProzent(80);
        vulkanier.setSchildeInProzent(80);
        vulkanier.setHuelleInProzent(50);
        vulkanier.setLebenserhaltungssystemeInProzent(100);
        vulkanier.setSchiffsname("Ni'Var");
        vulkanier.setReparaturBots(5);
        vulkanier.addLadung(vulk1);
		vulkanier.addLadung(vulk2);

        // Aufgabe 4.2.1
        // Klingonen schießen mit Photonentorpedo einmal auf Romulaner.
        System.out.println("**** Klingonen schießen auf Romulaner ****");
        klingonen.photonentorpedosSchiessen(romulaner);

        // Romulaner schießen mit Phaserkanone zurück.
        System.out.println("**** Romulaner schießen auf Klingonen ****");
        romulaner.phaserkanoneSchiessen(klingonen);

        // Vulkanier senden Nachricht an Alle "Gewalt ist nicht logisch".
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");

        // Klingonen rufen Zustand Ihres Raumschiffes ab und geben ihr
        // Ladungsverzeichnis aus

        System.out.println("**** Zustand & Ladungsverzeichnis ****");
        klingonen.zustandVonRaumschiff();
        klingonen.ladungsverzeichnisAusgabe();

        // Vulkanier setzen alle Androiden zur Aufwertung ihres Schiffes ein.
        System.out.println("**** Vulkanier schiffsaufwertung ****");
        vulkanier.reparaturDurchfuehrung(true, true, true, vulkanier.getreparaturBots());

        // Vulkanier verladen Ladung "Photonentorpedos" in Torpedoröhren des
        // Raumschiffes und räumen Ladungsverzeichnis auf

        System.out.println("**** Photonentorpedos Laden ****");
        vulkanier.photonentorpedosLadung(20);
        vulkanier.ladungsverzeichnisAufraeumen();

        // Klingonen schießen mit zwei weiteren Photonentorpedo auf Romulaner.
        System.out.println("**** Photonentorpedos 2x schießen ****");
        klingonen.photonentorpedosSchiessen(romulaner);
        klingonen.photonentorpedosSchiessen(romulaner);

        // Klingonen, Romulaner und Vulkanier rufen jeweils den Zustand
        // des Raumschiffes ab und geben ihr Ladungsverzeichnis aus

        System.out.println("**** Klingonen Zustand & Ladungsverzeichnis ****");
        klingonen.zustandVonRaumschiff();
        klingonen.ladungsverzeichnisAusgabe();

        System.out.println("**** Romulaner Zustand & Ladungsverzeichnis ****");
        romulaner.zustandVonRaumschiff();
        romulaner.ladungsverzeichnisAusgabe();

        System.out.println("**** Vulkanier Zustand & Ladungsverzeichnis ****");
        vulkanier.zustandVonRaumschiff();
        vulkanier.ladungsverzeichnisAusgabe();

        // Ausgabe von broadcastKommunikator
        System.out.println("**** broadcastKommunikator Ausgabe ****");

        ArrayList<String> broadcastKommunikator = new ArrayList<String>(); 
		broadcastKommunikator = klingonen.eintraegeLogbuch();
		System.out.println(broadcastKommunikator);
    }
}
