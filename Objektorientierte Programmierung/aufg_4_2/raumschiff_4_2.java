/**
 * @author David Krueger
 * @version 1.0
 */

import java.util.ArrayList;

public class Raumschiff_4_2 {
  // Attribute
  private int photonentorpedo;
  private int energieversorgungInProzent;
  private int schildeInProzent;
  private int huelleInProzent;
  private int lebenserhaltungssystemeInProzent;
  private int reparaturBots;
  private String schiffsname;
  private static ArrayList<Ladung_4_2> ladungsVerzeichnis = new ArrayList<Ladung_4_2>();
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();

  // Konstruktoren
  public Raumschiff_4_2() {}

  public Raumschiff_4_2(int photonentorpedo, int energieversorgungInProzent, int schildeInProzent, 
      int huelleInProzent, int lebenserhaltungssystemeInProzent, int reparaturBots, String schiffsname) {
          this.photonentorpedo = photonentorpedo; 
          this.energieversorgungInProzent = energieversorgungInProzent;
          this.schildeInProzent = schildeInProzent;
          this.huelleInProzent = huelleInProzent;
          this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
          this.reparaturBots = reparaturBots;
          this.schiffsname = schiffsname;
      }

  // Verwaltungsmethoden - setter
  public void setphotonentorpedo(int photonentorpedo) {
    this.photonentorpedo = photonentorpedo;
  }

  public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
    this.energieversorgungInProzent = energieversorgungInProzent;
  }

  public void setSchildeInProzent(int schildeInProzent) {
    this.schildeInProzent = schildeInProzent;
  }

  public void setHuelleInProzent(int huelleInProzent) {
    this.huelleInProzent = huelleInProzent;
  }

  public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
    this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
  }

  public void setReparaturBots(int reparaturBots) {
    this.reparaturBots = reparaturBots;
  }

  public void setSchiffsname(String schiffsname) {
    this.schiffsname = schiffsname;
  }

  // Verwaltungsmethoden - getter
  public int getphotonentorpedoan() {
    return this.photonentorpedo;
  }

  public int getEnergieversorgungInProzent() {
    return this.energieversorgungInProzent;
  }

  public int getSchildeInProzent() {
    return this.schildeInProzent;
  }

  public int getHuelleInProzent() {
    return this.huelleInProzent;
  }

  public int getLebenserhaltungssystemeInProzent() {
    return this.lebenserhaltungssystemeInProzent;
  }

  public int getreparaturBots() {
    return this.reparaturBots;
  }

  public String getSchiffsname() {
    return this.schiffsname;
  }

  // allgemeine Methoden

  /**
   * Addladung, Objekte "Ladung" hinzufügen
   * 
   * @param Ladung, Ladungsobjekt hinzufügen
   */
  public void addLadung(Ladung_4_2 ladung) {
    this.ladungsVerzeichnis.add(ladung); // ArrayList.add für das Hinzufügen der neuen Ladung genutzt
  }

  /**
   * Torpedos abfeuern
   * 
   * @param Raumschiff, Objekt als Ziel
   */
  public void photonentorpedosSchiessen(Raumschiff_4_2 r) {
    if (this.photonentorpedo > 0) { // Wenn das schießende Schiff Torpedos hat, Nachricht ausgeben
      this.nachrichtAnAlle("Photonentorpedo abgeschossen");
      r.getroffen(r); // Ziel "Objekt" Raumschiff aufrufen und die Methode "treffer()" ausführen
      this.photonentorpedo--;
    } else {
      System.out.println("-=Click=-"); // Keine Torpedos = Konsolenausgabe
    }
  }

  /**
   * Torpedos werden von Ladungsverzeichnis in Torpedoröhren gelden
   * 
   * @param int, Anzahl der Torpedos
   */
  public void photonentorpedosLadung(int anzahlTorpedos) {
    boolean hatTorpedos = false;
    int index = 0;
    int verladen = 0;

    // Ladungsverzeichnis wird durchsucht, ob Torpedos als Ladung existieren und ob
    // diese höher als 0 sind
    for (int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
      if (this.ladungsVerzeichnis.get(i).getBezeichnung() == "Photonentorpedo"
          && this.ladungsVerzeichnis.get(i).getMenge() > 0) {
        hatTorpedos = true;
        index = i;
      }
    }

    // Keine Torpedos, Nachricht ausgeben
    if (!hatTorpedos) {
      System.out.println("Photonentorpedos konnten nicht gefunden werden!");
      this.nachrichtAnAlle("-=*Click*=-");
    } else {
      // Wenn zu ladende Anzahl größer ist, als vorhanden = alle laden
      System.out.println("ja");
      if (anzahlTorpedos > this.ladungsVerzeichnis.get(index).getMenge()) {
        verladen = this.ladungsVerzeichnis.get(index).getMenge();
        this.photonentorpedo += verladen;
        this.ladungsVerzeichnis.get(index).setMenge(0);
      } else {
        verladen = this.ladungsVerzeichnis.get(index).getMenge();
        this.photonentorpedo += verladen;
        this.ladungsVerzeichnis.get(index).setMenge(this.ladungsVerzeichnis.get(index).getMenge() - anzahlTorpedos);
      }

      System.out.printf("[%d] Photonentorpedo(s) eingesetzt\n", verladen);
    }
  }

  /**
   * Phaserkanonen auf Schiff geschossen
   * 
   * @param Raumschiff, Objekt
   */
  public void phaserkanoneSchiessen(Raumschiff_4_2 r) {
    if (this.energieversorgungInProzent >= 50) { // Energie kleiner oder gleich 50 = neue Energie berechnung
      this.energieversorgungInProzent -= (int) (this.energieversorgungInProzent * 0.50);
      r.getroffen(r); // Treffer vermerkt
    } else {
      System.out.println("-=*Click*=-"); // Nicht genug energie, kein Treffer
    }
  }

  /**
   * Schiff wird getroffen
   * 
   * @param Raumschiff, Objekt
   */
  private void getroffen(Raumschiff_4_2 r) {
    // Schiff getroffen = Ausgabe, "r" ist das Ziel
    System.out.println(r.getSchiffsname() + " wurde getroffen!");

    // Erst Schutzschilde getroffen = Schwächung des getroffenen Schildes um 50%
    r.setSchildeInProzent((int) (r.getSchildeInProzent() * 0.50));

    // Schilde vollständig zerstört = Hülle und Energieversorgung jeweils um 50%
    // reduziert
    if (r.getSchildeInProzent() <= 0) {
      r.setEnergieversorgungInProzent((int) (r.getEnergieversorgungInProzent() * 0.50));
      r.setHuelleInProzent((int) (r.getHuelleInProzent() * 0.50));

      // Hülle auf 0% = Lebenserhaltungssysteme zerstört
      if (r.getHuelleInProzent() <= 0) {
        r.setLebenserhaltungssystemeInProzent(0);
        // Nachricht an Alle ausgegeben
        r.nachrichtAnAlle("Lebenserhaltungssysteme vollstädnig zerstört");
      }
    }
  }

  /**
   * An alle Raumschiffe eine Nachricht senden
   * 
   * @param String, Nachricht die alle Schiffe erhalten sollen
   */
  public void nachrichtAnAlle(String message) {
    this.broadcastKommunikator.add(message + "\n ");
  }

  /**
   * Einträge, die im Logbuch sind, werden zurürckgegeben
   * 
   * @return ArrayList<String>, alle Nachrichten in einer ArrayListe werden
   *         zurückgegeben
   */
  public static ArrayList<String> eintraegeLogbuch() {
    return broadcastKommunikator;
  }

  /*
   * Reparaturen der Schiffssysteme
   * 
   * @param boolean, Schutzschilde sollen repariert werden oder nicht
   * @param boolean, Energieversorung soll repariert werden oder nicht
   * @param boolean, Schiffshuelle soll repariert werden oder nicht
   * @param int,     Anzahl der reparaturdroiden die dafür benutzt werden soll
   */
  public void reparaturDurchfuehrung(boolean schildeInProzent, boolean energieversorgungInProzent, boolean huelleInProzent,
      int anzahlDroiden) {
    double myRandom = Math.random();
    int anzahl = 0;

    // Wenn mehr droiden im parameter angegeben, als das Schiff hat = Nutzung aller
    // droiden
    if (anzahlDroiden >= this.reparaturBots) {
      anzahlDroiden = this.reparaturBots;
    }

    // Prüfung, welche der parameter "true" sind
    // Hinzugefügt zur Anzahl für Rechnung der Reparatur
    if (schildeInProzent) {
      anzahl += 1;
    }

    if (energieversorgungInProzent) {
      anzahl += 1;
    }

    if (huelleInProzent) {
      anzahl += 1;
    }

    // Berechnung von Ergebnis, (Random Zahl * anzahl der Droiden) / Anzahl von
    // Systemen
    double ergebnis = (myRandom * anzahlDroiden) / anzahl;

    // Prüfung, welche der Parameter "true" sind / Addiere das ergebnis zu allen
    // "true" hinzu
    if (schildeInProzent) {
      this.schildeInProzent += ergebnis;
    }

    if (energieversorgungInProzent) {
      this.energieversorgungInProzent += ergebnis;
    }

    if (huelleInProzent) {
      this.huelleInProzent += ergebnis;
    }
  }

  /**
   * Ausgabe von Zustand des Raumschiffs mit printf in der Konsole
   */
  public void zustandVonRaumschiff() {
    System.out.printf(
        "Raumschiff %s \n"
            + "Energie: %s \n"
            + "Schilde: %s \n"
            + "Lebenserhaltung: %s \n"
            + "Huelle: %s \n"
            + "Torpedos: %s \n"
            + "Bots: %s \n",
        this.schiffsname,
        this.energieversorgungInProzent,
        this.schildeInProzent,
        this.lebenserhaltungssystemeInProzent,
        this.huelleInProzent,
        this.photonentorpedo,
        this.reparaturBots);
  }

  /**
   * Ausgabe von Ladungsverzeichnis des Raumschiffs mit printf in der Konsole
   */
  public void ladungsverzeichnisAusgabe() {
    System.out.println(this.ladungsVerzeichnis.toString());
  }

  /**
   * Ladungsverzeichnis wird durchsucht, wenn etwas mit menge=0 gefunden wird =
   * entfernt
   */
  public void ladungsverzeichnisAufraeumen() {
    for (int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
      if (this.ladungsVerzeichnis.get(i).getMenge() <= 0) {
        this.ladungsVerzeichnis.remove(i);
        i--;
      }
    }
  }
}
