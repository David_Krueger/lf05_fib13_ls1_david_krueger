/**
 * @author David Krueger
 * @version 1.0
 */

public class Ladung_4_2 {
    // Attribute
    private String bezeichnung;
    private int menge;

    // Konstruktoren
    public Ladung_4_2() {
    }

    public Ladung_4_2(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    // Verwaltungsmethoden - setter
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    // Verwaltungsmethoden - getter
    public String getBezeichnung() {
        return this.bezeichnung;
    }

    public int getMenge() {
        return this.menge;
    }

    // allgemeine Methoden

    /**
     * Override für Methode toString, custom Ausgabe für Objekt
     * 
     * @return String, alle Attribute der Klasse
     */
    @Override
    public String toString() {
        return "CName: " + Ladung_4_2.class.getName() + " \nBezeichnung: " + this.bezeichnung + " \nMenge: " + this.menge
                + " \n";
    }
}