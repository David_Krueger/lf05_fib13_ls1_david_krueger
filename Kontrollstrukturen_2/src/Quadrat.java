import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		int eingabe;
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Bitte bestimmen Sie die Seitenl�nge des Quadrats: ");
		eingabe = myScanner.nextInt();

		System.out.println("Sie haben die Seitenl�nge wie folgt bestimmt: " + eingabe
				+ "\nDas Quadrat wird nun dargestellt.\n");
		for (int i = 1; i <= eingabe; i++) {
			System.out.print("* ");
		}
		System.out.println("");
		for (int i = 1; i <= eingabe - 2; i++) {
			System.out.print("* ");
			for (int j = 1; j <= eingabe - 2; j++) {
				System.out.print("  ");
			}
			System.out.println("* ");
		}
		for (int i = 1; i <= eingabe; i++) {
			System.out.print("* ");
		}

	}

}
