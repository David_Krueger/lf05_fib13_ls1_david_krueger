import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {
		String eingabe;
		double r;
		double i;
		double u;
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Bitte Wählen Sie ein Formelzeichen aus. "
				+ "\n\nZur Auswahl stehen: "
				+ "\n\n- Das Formelzeichen >> U << für die Spannung. Wählen Sie bitte die >> 1 <<"
				+ "\n- Das Formelzeichen >> R << für den Widerstand. Wählen Sie bitte die >> 2 <<"
				+ "\n- Das Formelzeichen >> I << für den Strom. Wählen Sie bitte die >> 3 <<");
		eingabe = myScanner.next();

		switch (eingabe) {
			case "1":
				System.out.println("Sie haben das Formelzeichen >> U << gewählt."
						+ "\nBitte geben Sie nun den Wert für >> R << ein: ");
				r = myScanner.nextDouble();
				System.out.println("Bitte geben Sie nun den Wert für >> I << ein: ");
				i = myScanner.nextDouble();
				System.out.printf("\nDas Ergebnis von >> U << beträgt: " + "%.2f", r * i);
				break;
			case "2":
				System.out.println("Sie haben das Formelzeichen >> R << gewählt."
						+ "\nBitte geben Sie nun den Wert für >> U << ein: ");
				u = myScanner.nextDouble();
				System.out.println("Bitte geben Sie nun den Wert für >> I << ein: ");
				i = myScanner.nextDouble();
				System.out.printf("\nDas Ergebnis von >> R << beträgt: " + "%.2f", u / i);
				break;
			case "3":
				System.out.println("Sie haben das Formelzeichen >> I << gewählt."
						+ "\nBitte geben Sie nun den Wert für >> U << ein: ");
				u = myScanner.nextDouble();
				System.out.println("Bitte geben Sie nun den Wert für >> R << ein: ");
				r = myScanner.nextDouble();
				System.out.printf("\nDas Ergebnis von >> I << beträgt: " + "%.2f", u / r);
				break;

			default:
				System.out.println("Fehler. Bitte versuchen Sie es erneut.");
		}
	}

}
