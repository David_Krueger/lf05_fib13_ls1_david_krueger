import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		double a;
		double b;
		String rechenoption;

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Taschenrechner aktiv");
		System.out.println("\nBitte geben Sie einen Wert ein.");
		a = myScanner.nextDouble();
		System.out.println("Bitte geben Sie einen weiteren Wert ein.");
		b = myScanner.nextDouble();
		System.out.println("W�hlen Sie f�r die Berechnung einen Operator aus: + , - , * , / ");
		rechenoption = myScanner.next();

		switch (rechenoption) {
			case "+":
				System.out.printf("\nDas Ergebnis lautet: " + "%.2f", a + b);
				break;
			case "-":
				System.out.printf("\nDas Ergebnis lautet: " + "%.2f", a - b);
				break;
			case "*":
				System.out.printf("\nDas Ergebnis lautet: " + "%.2f", a * b);
				break;
			case "/":
				System.out.printf("\nDas Ergebnis lautet: " + "%.2f", a / b);
				break;
			default:
				System.out.println("Fehler. Bitte versuchen Sie es erneut.");

		}
	}

}
