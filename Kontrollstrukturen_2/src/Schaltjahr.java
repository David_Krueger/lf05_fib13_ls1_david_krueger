import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		int jahreszahl;

		Scanner myScanner = new Scanner(System.in);
		System.out.println("W�hlen Sie eine Jahreszahl und geben diese bitte ein: ");

		jahreszahl = myScanner.nextInt();

		if (jahreszahl % 4 == 0) {
			if (jahreszahl % 100 == 0) {
				if (jahreszahl % 400 == 0) {
					System.out.println("Das von Ihnen eingegebene Jahr ist ein Schaltjahr.");
				} else {
					System.out.println("Das von Ihnen eingegebene Jahr ist kein Schaltjahr.");
				}
			} else {
				System.out.println("Das von Ihnen eingegebene Jahr ist ein Schaltjahr.");
			}
		} else {
			System.out.println("Das von Ihnen eingegene Jahr ist kein Schaltjahr.");
		}
	}

}
