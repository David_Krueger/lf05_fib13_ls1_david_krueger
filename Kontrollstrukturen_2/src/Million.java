import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		double einlage;
		double zinssatz;
		double summe = 0;
		int jahreszahl = 0;
		String eingabe;
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Dies ist ein Programm, welches die Anzahl der Jahre berechnet, bis Sie aufgrund "
				+ "\neiner einmaligen Einlage und einem konstanten Zinssatz Millionär geworden sind"
				+ "\n\nUm das Programm zu starten, wählen Sie bitte die Ziffer >> 1 <<"
				+ "\nUm das Programm zu beenden, wählen Sie bitte die Ziffer >> 2 <<");
		eingabe = myScanner.next();

		while (eingabe.equals("1") && eingabe != "2") {
			System.out.println("Bitte geben Sie die Höhe der Einlage ein: ");
			einlage = myScanner.nextDouble();
			System.out.println("Bitte geben Sie die Höhe des Zinssatzes ein: ");
			zinssatz = myScanner.nextDouble();

			zinssatz = zinssatz / 100;
			summe = einlage;

			while (summe < 1000000) {
				summe += summe * zinssatz;
				jahreszahl++;
			}
			System.out.println("Glückwunsch. Sie sind in " + jahreszahl + " Jahren ein Millionär.");
			System.out.println("\nMöchten Sie das Programm weiterhin nutzen? Wählen Sie bitte die Ziffer >> 1 <<"
					+ "\nMöchten Sie das Programm beenden? Wählen Sie bitte die Ziffer >> 2 <<");
			eingabe = myScanner.next();
			if (eingabe.equals("2")) {
				System.out.println("Sie haben das Progamm beendet.");
				break;
			} else if (eingabe.equals("1")) {

			} else {
				System.out.println("Fehler. Bitte starten Sie das Programm erneut.");
				break;
			}
		}
	}

}
