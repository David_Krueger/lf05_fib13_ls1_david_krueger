import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		int gewicht;
		double koerpergroesse;
		String geschlecht;
		double summe;

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie Ihr Gewicht an: ");
		gewicht = myScanner.nextInt();

		System.out.println("Bitte geben Sie Ihre Körpergröße in m an: ");
		koerpergroesse = myScanner.nextDouble();
		koerpergroesse = koerpergroesse / 100;

		System.out.println("Sind Sie männlich, dann wählen Sie die Ziffer >> 1 <<"
				+ "\nSind Sie weiblich, dann wählen Sie die Ziffer >> 2 <<");
		geschlecht = myScanner.next();

		System.out.printf("Der errechnete BMI lautet: ");
		summe = (bmi(gewicht, koerpergroesse));
		System.out.println(summe);

		if (geschlecht.equals("1")) {
			if (summe < 20) {
				System.out.println("Achtung! Sie befinden sich aktuell im Untergewicht.");
			} else if (summe >= 20 && summe <= 25) {
				System.out.println("Sie befinden sich aktuell im Normalgewicht.");
			} else {
				System.out.println("Achtung! Sie befinden sich aktuell im Übergewicht.");
			}
		}

		if (geschlecht.equals("2")) {
			if (summe < 19) {
				System.out.println("Achtung! Sie befinden sich aktuell im Untergewicht.");
			} else if (summe >= 19 && summe <= 24) {
				System.out.println("Sie befinden sich aktuell im Normalgewicht.");
			} else {
				System.out.println("Achtung! Sie befinden sich aktuell im Übergewicht.");
			}
		}
	}

	static double bmi(double a, double b) {
		double bmi = (a / (b * b));
		return bmi;
	}

}
