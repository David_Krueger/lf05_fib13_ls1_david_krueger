import java.util.*;

public class Palindrom {

	public static void main(String[] args) {
		// Im Programm „Palindrom“ werden per Eingabe 5 Zeichen eingelesen
		// Anschließend in geeignetes Array gespeichert.
		// Danach wird der Inhalt des Arrays von hinten nach vorn ausgegeben

		try (Scanner tastatur = new Scanner(System.in)) {
			System.out.println("Bitte geben Sie ein Zeichen ein: ");
			String eingabe = tastatur.next();
			char[] eingabeZeichen = eingabe.toCharArray();

			System.out.println("Palindrom ausgegeben: ");
			for (int i = eingabeZeichen.length; i <= eingabeZeichen.length && i != 0; i--) {
				System.out.print(eingabeZeichen[i - 1] + "");
			}
		}

	}

}