// Name: David Krüger
// Klasse: FI-B 13

import java.util.Scanner; // Importierung Scanner

public class Aufgabe3_3__David_Krüger__FI_B_13 {
	public static void main(String[] args) { // Standardgerüst

		double zuZahlenderBetrag; // Deklarierung Variable "zuZahlenderBetrag" als Double (= Gleitkommazahl)
		double rueckgabeBetrag; // Deklarierung Variable "rueckgabeBetrag" als Double (= Gleitkommazahl)
		double anzahl; // Deklarierung Variable "anzahl" als Double (= Gleitkommazahl)

		// Argumente = Werte oder Referenzen, die beim Aufruf an die Funktion übergeben
		// werden

		anzahl = ticketAnzahl(); // Variable "anzahl" wird "ticketAnzahl" als Funktionsaufruf zugewiesen
		zuZahlenderBetrag = fahrkartenbestellungErfassen(anzahl); // Variable "zuZahlenderBetrag" wird
																	// "fahrkartenbestellungErfassen" als
																	// Funktionsaufruf und "anzahl" als Argument
																	// zugewiesen
		rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag); // Variable "rueckgabeBetrag" wird "fahrkartenBezahlen"
																	// als Funktionsaufruf und "zuZahlenderBetrag" als
																	// Argument zugewiesen
		fahrkartenAusgeben(anzahl); // Funktionsaufruf "fahrkartenAusgeben" wird "anzahl" als Argument zugewiesen
		rueckgeldAusgeben(rueckgabeBetrag, anzahl); // Funktionsaufruf "rueckgeldAusgeben" werden "rueckgabeBetrag" und
													// "anzahl" als Argumente zugewiesen
	}

	// Die Anzahl der Tickets werden gezählt und folgend ausgegeben.
	// Voraussetzung, da in verschiedenen Methoden anzahlTickets benötigt werden.

	public static double ticketAnzahl() { // Methode "ticketAnzahl" mit double als Rückgabewert
		Scanner tastatur = new Scanner(System.in); // Erstellung Scanner "tastatur" -> Wert durch Eingabe bestimmt
		double anzahl; // Variable "anzahl" als double deklariert
		System.out.println("Anzahl der Tickets: "); // Gebe Text aus...
		anzahl = tastatur.nextDouble(); // Variable "anzahl" durch Eingabe bestimmt

		return anzahl; // "anzahl" rückgeben
	}

	// Multiplizierung "ticketAnzahl" mit "zuZahlenderBetrag"

	public static double fahrkartenbestellungErfassen(double x) { // Methode "fahrkartenbestellungErfassen" mit double
																	// als Rückgabewert
		Scanner tastatur = new Scanner(System.in); // Verwendung Scanner
		double preis; // Variable "preis" als double bestimmt
		System.out.println("Ticketpreis (EURO): "); // Gebe Text aus
		preis = tastatur.nextDouble(); // "preis" durch Eingabe bestimmt

		return (x * preis); // "x" mit "preis" multiplizieren und folgend rückgeben
	}

	// Kunde wird informiert, was gezahlt werden soll und was eingezahlt worden ist.

	public static double fahrkartenBezahlen(double x) { // Methode "fahrkartenBezahlen" mit double als Rückgabewert
		Scanner tastatur = new Scanner(System.in); // Verwendung Scanner
		double geld; // "geld" als double ausgeben
		System.out.printf("Noch zu zahlen: " + "%.2f EURO \n", x); // Gebe Text aus...
		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): "); // Gebe Text aus...

		while (x > 0.0) {
			geld = tastatur.nextDouble(); // "geld" durch Eingabe bestimmt
			x = x - geld; // Betrag, der noch gezahlt werden muss verringert sich durch eingezahltes Geld

			if (x > 0.0) // so lange einzahlen, bis unter 0.0
			{
				System.out.printf("\nSie haben " + "%.2f EURO eingezahlt.\n", +geld); // Gebe Text aus...
				System.out.printf("Noch zu zahlen: " + "%.2f EURO \n", x); // Gebe Text aus...
			}
		}
		if (x < 0.0) // wenn "x" kleiner als 0.0
		{
			x = (x * -1);
			System.out.printf("\nAchtung: Sie haben " + "%.2f", x); // Gebe Text aus...
			System.out.println(" EURO zu viel gezahlt."); // Gebe Text aus...
		}
		return x; // "x" rückgeben
	}

	// Kunde wird informiert, dass das Ticket gedruckt wird.

	public static void fahrkartenAusgeben(double x) { // Methode "fahrkartenAusgeben" mit double als Rückgabewert

		if (x <= 1) // wenn "x" kleiner als oder gleich 1
		{
			System.out.println("\nFahrschein wird gedruckt\n"); // Gebe Text aus...
		} else // oder...
		{
			System.out.printf("\n" + "%.0f" + " Fahrscheine werden gedruckt\n\n", x); // Gebe Text aus...
		}
		for (int i = 0; i < 8; i++) // Variable "i" als Integer deklariert, Wert 0 (int i = 0) -> solange i < 8 ist,
									// Wert 1 hinzunehmen (i++)
		{
			System.out.print("="); // Gebe Text aus...
			try {
				Thread.sleep(250); // Verzögerte Ausgabe, damit Fortschrittsbalken visualisiert wird
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	// Ausgabe des Rückgabebetrages und Informationen an den Kunden

	public static void rueckgeldAusgeben(double x, double y) { // Methode "rueckgeldAusgeben" mit double x, double y als
																// Rückgabewert

		if (x > 0.0) // wenn "x" größer als 0.0
		{
			System.out.printf("\n\nDer Rückgabebetrag in Höhe von" + " %.2f EURO ", (x)); // Gebe Text aus...
			System.out.println("\nwird in folgenden Münzen ausgezahlt:"); // Gebe Text aus...

			while (x >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO"); // Gebe Text aus...
				x -= 2.0;
			}
			while (x >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO"); // Gebe Text aus...
				x -= 1.0;
			}
			while (x >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT"); // Gebe Text aus...
				x -= 0.5;
			}
			while (x >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT"); // Gebe Text aus...
				x -= 0.2;
			}
			while (x >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT"); // Gebe Text aus...
				x -= 0.1;
			}
			while (x >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT"); // Gebe Text aus...
				x -= 0.05;
			}
		}
		if (y <= 1) // wenn "y" kleiner als oder gleich 1
		{
			System.out.println("\nVergessen Sie nicht, den Fahrschein"); // Gebe Text aus...
		} else // oder...
		{
			System.out.println("\nVergessen Sie nicht, die Fahrscheine"); // Gebe Text aus...
		}
		System.out.println("vor Fahrtantritt entwerten zu lassen!\n" + // Gebe Text aus...
				"\nWir wünschen Ihnen eine gute Fahrt."); // Gebe Text aus...
	}
}