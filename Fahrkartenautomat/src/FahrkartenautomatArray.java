// Name: David Kr�ger
// Klasse: FI-B 13

import java.util.Scanner;

class FahrkartenautomatArray {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		String option;
		Scanner op = new Scanner(System.in);

		do {
			// Auslagerung der Kernstruktur des Fahrkartenautomats in Methoden
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n");

			// Soll ein weiterer durchlauf durchgef�hrt werden?
			System.out.println("Wollen Sie einen erneuten Durchlauf starten? (J/N)");
			option = op.next();

		} while (option.contains("J") || option.contains("j"));

		System.out.println(" >>> Programm beendet <<< ");
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		boolean bezahlen = false;
		byte ticketAuswahl;
		byte ticketAnzahl = 0;
		double gesamtpreis = 0.0;

		// Aufgabe 1: Welche Vorteile hat man durch diesen Schritt?:
		//
		// Beschreibung[i] = Preis[i] -> einfache Zugeh�rigkeit
		// Im Code eine zentrale Stelle, wo Anspassungen vorgenommen werden k�nnen
		// Dynamisch, weil: => Gr��e der Arrays kann ver�ndert werden, ohne das die
		// Funktionalit�t beeintr�chtigt wird
		//
		// Aufgabe 3: Vergleichen Sie die neue Implementierung mit der alten und
		// erl�utern Sie die Vor- und Nachteile der jeweiligen Implementierung:
		//
		// Dynamisch, weil: => Gr��e der Arrays kann ver�ndert werden, ohne das die
		// Funktionalit�t beeintr�chtigt wird
		// In beiden Arrays an Position[i] ist der Name der Tickets und der Preis der
		// Tickets zu finden
		// Gr��e von Arrays = ungleich => Fehler
		// Bei einer hohen Anzahl von Tickets kann die �bersichtlichkeit unter Umst�nden
		// schnell verloren gehen
		// => Folge: Stelle wo der Preis eingef�gt werden soll ist unklar

		String[] ticketBeschreibung = {
				"- Einzelfahrschein Berlin AB", "- Einzelfahrschein Berlin BC", "- Einzelfahrschein Berlin ABC",
				"- Kurzstrecke", "- Tageskarte Berlin AB", "- Tageskarte Berlin BC", "- Tageskarte Berlin ABC",
				"- Kleingruppen-Tageskarte Berlin AB", "- Kleingruppen-Tageskarte Berlin BC",
				"- Kleingruppen-Tageskarte Berlin ABC" };
		double[] ticketPreise = {
				2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };

		System.out.println("Willkommen am Fahrkartenautomaten der BVG. Welche Fahrkarte m�chten Sie gerne l�sen?");
		System.out.println("\n======================================================================================");
		System.out.println("");

		// Loop f�r die Bestellung der Tickets
		do {
			System.out.println("Bitte w�hlen Sie eine Ziffer zwischen (1) und (11)\n ");

			// Ausgabe Elemente der einzelnen Arrays und Bezahloption
			for (int i = 0; i < ticketPreise.length; i++) {
				System.out.printf("  %s [%.2f EUR] (%d)\n", ticketBeschreibung[i], ticketPreise[i], i + 1);
			}
			System.out.printf("\n  - Bezahlen (%d)\n", ticketPreise.length + 1);

			// Ticketauswahl
			System.out.println(
					"\n======================================================================================");
			System.out.println("");
			System.out.print("Ziffer: ");
			ticketAuswahl = tastatur.nextByte();

			// "Bezahlen" ausgew�hlt => "bezahlen" auf true setzen
			if (ticketAuswahl == ticketPreise.length + 1) {
				bezahlen = true;
			}

			// Auswahlm�glichkeiten nur in bstmt. Range vom Array anbieten
			while (ticketAuswahl <= 0 || ticketAuswahl > ticketPreise.length + 1) {
				System.out.println(" >>falsche Eingabe<< ");
				System.out.print("Ihre Wahl: ");
				ticketAuswahl = tastatur.nextByte();
			}

			// Tickets nur ausw�hlen, sofern nicht bezahlt werden soll
			if (bezahlen != true) {
				System.out.print("Anzahl der Tickets: ");
				ticketAnzahl = tastatur.nextByte();
			}

			// Ticketanzahl darf nur zwischen 0 und 10 sein
			while (ticketAnzahl > 10 || ticketAnzahl < 0) {
				System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
				System.out.print("Anzahl der Tickets: ");
				ticketAnzahl = tastatur.nextByte();
			}

			// Nur Zwischensumme und Preis bestimmen wenn Ticket ausgew�hlt wurde
			if (bezahlen != true) {
				gesamtpreis += ticketPreise[ticketAuswahl - 1] * ticketAnzahl;
				System.out.printf("Zwischensumme: %.2f �\n", gesamtpreis);
			}

			// Zeilenumbruch
			System.out.println("");
		} while (bezahlen != true);

		return gesamtpreis;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		String einheit = "Euro";
		muenzeAusgeben(rueckgabebetrag, einheit);
	}

	// Verz�gerung, um Ticketausgabe zu Simulieren
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Ausgabe der M�nzen in eigener Methode
	public static void muenzeAusgeben(double betrag, String einheit) {
		if (betrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f %s \n", betrag, einheit);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (betrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.printf("2 %s \n", einheit);
				betrag -= 2.0;
			}
			while (betrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.printf("1 %s \n", einheit);
				betrag -= 1.0;
			}
			while (betrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.printf("0.50 %s \n", einheit);
				betrag -= 0.5;
			}
			while (betrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.printf("0.20 %s \n", einheit);
				betrag -= 0.2;
			}
			while (betrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.printf("0.10 %s \n", einheit);
				betrag -= 0.1;
			}
			while (betrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.printf("0.05 %s \n", einheit);
				betrag -= 0.05;
			}
		}
	}
}