﻿import java.util.Scanner; // Importierung Scanner

class Aufgabe2_5final {
    public static void main(String[] args) // Standardgerüst
    {
        Scanner tastatur = new Scanner(System.in); // Erstellung Scanner "tastatur" -> Wert durch Eingabe bestimmt

        double zuZahlenderBetrag; // Deklarierung Variable "zuZahlenderBetrag" als Double (= Gleitkommazahl)
        double eingezahlterGesamtbetrag; // Deklarierung Variable "eingezahlterGesamtbetrag" als Double (=
                                         // Gleitkommazahl)
        double eingeworfeneMuenze; // Deklarierung Variable "eingeworfeneMünze" als Double (= Gleitkommazahl)
        double rueckgabebetrag; // Deklarierung Vaiable "rückgabebetrag" als Double (= Gleitkommazahl)
        short anzahlTickets; // Deklarierung Variable "anzahlTickets" als Short (= Ganzzahl von -32768 bis
                             // +32767)
        float ticketPreis; // Deklarierung Variable "ticketPreis" als Float (= Gleitkommazahl)

        System.out.print("Ticketpreis (EURO): "); // Gebe Text aus
        ticketPreis = tastatur.nextFloat(); // Betrag für ein Tickets durch Eingabe bestimmen

        System.out.print("Anzahl der Tickets: "); // Gebe Text aus
        anzahlTickets = tastatur.nextShort(); // Anzahl der benötigten Tickets durch Eingabe bestimmen

        zuZahlenderBetrag = anzahlTickets * ticketPreis; // Gesamtbetrag ergibt sich durch Multiplikation der Tickets
                                                         // mit dem Einzelpreis

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0; // Variable als Zähler (Schleife), startet bei 0.0
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) // Boolsche Abfrage: eingezahlterGesamtbetrag <
                                                             // zuZahlenderBetrag -> wahr?
        {
            System.out.printf("Noch zu zahlen: " + "%.2f EURO \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag)); // Subtraktion:
                                                                                                                    // zuZahlenderBetrag
                                                                                                                    // -
                                                                                                                    // eingezahlterGesamtbetrag
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): "); // Gebe Text aus
            eingeworfeneMuenze = tastatur.nextDouble(); // "eingeworfeneMünze" durch Eingabe bestimmen
            eingezahlterGesamtbetrag += eingeworfeneMuenze; // Addition: eingezahlterGesamtbetrag += eingeworfeneMuenze
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein(e) wird/werden ausgegeben"); // Gebe Text aus
        for (int i = 0; i < 8; i++) // Variable "i" als Integer deklariert, Wert 0 (int i = 0) -> solange i < 8 ist,
                                    // Wert 1 hinzunehmen (i++)
        {
            System.out.print("="); // Gebe Text aus
            try {
                Thread.sleep(250); // Verzögerte Ausgabe, damit Fortschrittsbalken visualisiert wird
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n"); // Gebe Text aus

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; // Subtraktion: eingezahlterGesamtbetrag -
                                                                        // zuZahlenderBetrag
        if (rueckgabebetrag > 0.0) // Wenn "rückgabebetrag" größer als 0,0 dann...
        {
            System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f EURO \n", rueckgabebetrag); // Gebe Text +
                                                                                                    // Variable aus
            System.out.println("wird in folgenden Münzen ausgezahlt:"); // Gebe Text aus

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen // Boolsche Abfrage: "rückgabebetrag" größer oder gleich
                                           // 2,0? -> Dann...
            {
                System.out.println("2 EURO"); // Gebe Text aus
                rueckgabebetrag -= 2.0; // Ziehe 2.0 von Variable "rückgabebetrag" ab
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen // Boolsche Abfrage
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen // Boolsche Abfrage
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen // Boolsche Abfrage
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen // Boolsche Abfrage
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen // Boolsche Abfrage
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + // Gebe Text aus
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

// Aufgabe 5: Für die Variable "anzahlTickets" habe ich den Datentyp "Short"
// gewählt und 2 Byte Speicherplatz in Kauf genommen,
// da ich an eine Großbestellung von Tickets durch bspw. mehreren
// Klassenverbänden dachte. Da der Datentyp "Byte" nur Werte bis
// maximal "127" ausgeben kann, erschien mir dieser folgend als nicht geeignet.
// Aufgabe 5: Für die Variable "ticketPreis" habe ich den Datentyp "Float"
// gewählt, da dieser weniger Speicherplatz im Vergleich
// zum Double in Anspruch nimmt. Der Datentyp "Float" kann acht Dezimalstellen
// ausgeben. Ich bin davon ausgegangen, dass der Preis
// eines Einzeltickets acht Dezimalstellen nicht übersteigen sollte.

// Aufgabe 6: Bei dem Ausdruck anzahl * einzelpreis werden zwei Glieder durch
// den Multiplikationsopperator miteinander multipliziert.
// Konkret bedeutet das in unserer Aufgabe, dass die Anzahl der Tickets (Varible
// = "anzahlTickets"), die gekauft werden möchten
// mit dem Preis eines einzelnen Tickets (Variable = ticketPreis) multipliziert
// werden. Durch die Multiplikation erhält man den
// tatsächlichen Betrag, der am Ende gezahlt werden muss. Wichtig: Es werden
// zuerst die Werte der Variablen "ticketPreis" und "anzahlTickets"
// verrechnet und erst danach wird der "zuZahlendeBetrag" initialisiert!