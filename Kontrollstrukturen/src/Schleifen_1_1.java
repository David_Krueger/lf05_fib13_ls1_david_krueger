
import java.util.Scanner;

public class Schleifen_1_1 {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Bitte bestimmen Sie den Wert f�r ''n'': ");
		int eingabe = tastatur.nextInt();

		for (int i = 1; i <= eingabe; i++) {
			if (i != eingabe) {
				System.out.print(i + ", ");
			} else
				System.out.print(i);

		}
		System.out.println("\n");

		for (int i = eingabe; i >= 1; i--) {
			if (i <= eingabe && i > 1) {
				System.out.print(i + ", ");
			}

			else
				System.out.print(i);

		}
	}
}
