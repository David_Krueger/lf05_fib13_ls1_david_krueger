import java.util.Scanner;

public class Schleifen_1_2 {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		int n = tastatur.nextInt();

		int i = 0;
		int summe = 0;
		while (i != n) {
			i++;
			summe += i;
			System.out.println("i= " + i);
		}
		System.out.println("Summe:" + summe);

		System.out.println(" ");

		int j = 0;
		int summe1 = 0;
		while (j <= 2 * n) {
			j = j + 2;
			summe1 += j;
			System.out.println("j= " + j);
		}

		System.out.println("Summe: " + summe1);

		System.out.println(" ");

		int k = 0;
		int summe2 = 0;
		while (k <= 2 * n + 1) {
			k = k + 3;
			summe2 += k;
			System.out.println("k= " + k);
		}
		System.out.println("Summe: " + summe2);

		System.out.println(" ");
	}
}
