
public class Aufgabe1_5_3 {

	public static void main(String[] args) {
		// "Aufgabe 1.5.3" ausgeben
		
		System.out.println("Aufgabe 1.5.3");
		
		// "Absatz" ausgeben
	    System.out.println(" ");
		
		double d = 22.4234234;
		double d1 = 111.2222;
		double d2 = 4.0;
		double d3 = 1000000.551;
		double d4 = 97.34;
		
		System.out.printf("%.2f\n" , d);
		System.out.printf("%.2f\n" , d1);
		System.out.printf("%.2f\n" , d2);
		System.out.printf("%.2f\n" , d3);
		System.out.printf("%.2f\n" , d4);
		
		

	}

}
