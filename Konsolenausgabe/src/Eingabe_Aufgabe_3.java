import java.util.Scanner;

public class Eingabe_Aufgabe_3 {

	public static void main(String[] args) {
		// Scanner erstellen
		Scanner myScanner = new Scanner(System.in);

		// Begr��en und nach Namen fragen:
		System.out.println("Guten Tag! Wie ist Ihr Name? Geben Sie ihn bitte ein:");

		String name = myScanner.next();

		// Alter eingeben
		System.out.println("Bitte geben Sie ihr Alter ein:");

		int alter = myScanner.nextInt();

		System.out.println("Sie hei�en " + name + "und sind " + alter + " Jahre alt.");

	}

}
