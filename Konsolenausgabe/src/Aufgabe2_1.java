
public class Aufgabe2_1 {

	public static void main(String[] args) {
		// Aufagbe 2_1 ausgeben
		System.out.println("Aufgabe 2_1");
		System.out.println(" ");
		
		// Zwein obere Sterne ausgeben **
		System.out.println("    **  "); 
		
		// Vier Sterne, jeweils vertikal ausgeben
		System.out.println("*        *");
		System.out.println("*        *");
		
		// Zwei untere Sterne horizontal ausgeben 
		System.out.print("    **  ");
		
		// Leerzeilen ausgeben
		System.out.println(" ");
		System.out.println(" ");
		
		// Gleiche Aufgabe ohne Leerzeichen ausgeben
		System.out.println("Gleiche Aufgabe, aber jetzt ohne Leerzeichen");
		System.out.println(" ");
		
		// Festlegung einzelner Strings, die in Funktion eingesetzt werden sollen
		
		String A = "**";
		String B1 = "*";
		String B2 = "*";
		String C1 = "*"; 
		String C2 = "*";
		String D = "**";
		
		// Zeilen ausgeben
		System.out.printf("%6s%n", A);
		System.out.printf("%s", B1);
		System.out.printf("%9s%n", B2);
		System.out.printf("%s", C1);
		System.out.printf("%9s%n", C2);
		System.out.printf("%6s", D);
	}

}
