
public class Aufgabe1_5_1 {

	public static void main(String[] args) {
		// "Erster Teil Aufg. 1.5.1" ausgeben
		System.out.print("Erster Teil Aufg. 1.5.1");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		System.out.println(" ");
		
		//"Mein Auto ist gr�n" ausgeben
		System.out.print("Mein Auto ist gr�n.");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		
		// "Fabian kann kein Auto fahren" ausgeben
		System.out.print("Fabian kann kein Auto fahren");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		System.out.println(" ");
		
		// "Zweiter Teil Aufg. 1.5.1" ausgeben
		System.out.print("Zweiter Teil Aufg. 1.5.1");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		System.out.println(" ");
		
		// "Mein Auto ist gr�n" ausgeben und einen Zeilenvorschub ausf�hren
		System.out.print("Mein Auto ist gr�n\n");
		
		// "Fabian kann kein Auto fahren"
		System.out.println("Fabian kann \"kein\" Auto fahren");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		
		// "Dritter Teil Aufg. 1.5.1" ausgeben
		System.out.print("Dritter Teil Aufg. 1.5.1");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		System.out.println(" ");
		
		//"Ich hei�e David und bin 21 Jahre alt" ausgeben
		int alter = 21;
		String name = "David";
		System.out.print("Ich hei�e "+ name + " und bin " + alter  + " Jahre alt.");
		
		// "Absatz" ausgeben
		System.out.println(" ");
		System.out.println(" ");
		
		
		
		
		
		
		

	}

}
