
public class Aufgabe2_3 {
	
	public static void main(String[] args) {

	// Bestimmung einzelner Variablen f�r Einsetzung der entsprechenden Funktionen:
		// String X = Fahrenheit:
		// double X = Celsius:
		
	String F = "Fahrenheit";
	String C = "Celsius";
	
	String A1 = "-20";
	double A2 = -28.8889;
	
	String B1 = "-10";
	double B2 = -23.3333;
	
	String C1 = "0";
	double C2 = -17.7778;
	
	String D1 = "20";
	double D2 = -6.6667;
	
	String E1 = "30";
	double E2 = -1.1111;
	
	// Tabellenzeilen/ Tabellenspalten ausgeben:
	
		// Zeile 1 ausgeben:
	
	// Fahreneinheit ausgeben:
	System.out.printf("%-12s|", F);
	
	// Celsius ausgeben:
	System.out.printf("%10s%n", C);
			
	// Tabellenbegrenzung (Linie) ausgeben:
	System.out.println("------------------------");
	
		// Zeile 2 ausgeben:
	
	// -20 ausgeben:
	System.out.printf("%-12s|", A1);
		
	// -28,89 ausgeben:
	System.out.printf("%10.2f%n", A2); 
	
		// Zeile 3 ausgeben:
	
	// -10 ausgeben:
	System.out.printf("%-12s|", B1);
	
	// -23,33 ausgeben:
	System.out.printf("%10.2f%n", B2);
	
		// Zeile 4 ausgeben
	
	// +0 ausgeben:
	System.out.printf("+%-11s|", C1);
	
	// -17,78 ausgeben:
	System.out.printf("%10.2f%n", C2);
	
		// Zeile 5 ausgeben:
	
	// +20 ausgeben:
	System.out.printf("+%-11s|", D1);
	
	// -6,67 ausgeben:
	System.out.printf("%10.2f%n", D2);
	
		// Zeile 6 ausgeben:
	
	// +30 ausgeben:
	System.out.printf("+%-11s|", E1);
	
	// -1,11 ausgeben:
	System.out.printf("%10.2f%n",  E2);
	
	}
}
